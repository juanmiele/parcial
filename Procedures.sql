USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[CONCEPTO_BORRAR]    Script Date: 30/9/2020 11:26:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[CONCEPTO_BORRAR]
@codigo int
as
begin

delete from Concepto 
where
Codigo = @codigo

end
GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[CONCEPTO_BUSCAR]    Script Date: 30/9/2020 11:26:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CONCEPTO_BUSCAR]
@retencion bit
as
BEGIN
SELECT * FROM Concepto where Retencion =@retencion
END 

GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[CONCEPTO_EDITAR]    Script Date: 30/9/2020 11:26:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CONCEPTO_EDITAR]
		@codigo int,
           @Nombre nchar(10),
           @porcentaje int,
           @Retencion bit
as
BEGIN 



update Concepto set Nombre=@Nombre,
porcentaje=@porcentaje,
Retencion=@Retencion

where
Codigo = @codigo 



END
GO
USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[CONCEPTO_INSERTAR]    Script Date: 30/9/2020 11:26:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CONCEPTO_INSERTAR]
           @Nombre nchar(10),
           @porcentaje int,
           @Retencion bit
as
BEGIN 
declare @codigo int

set @codigo  = isnull( (SELECT MAX(Codigo) from Concepto) ,0) +1

insert Concepto values (@codigo,@Nombre,@porcentaje,@Retencion)


END
GO
USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[CONCEPTO_LISTAR]    Script Date: 30/9/2020 11:27:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CONCEPTO_LISTAR]
as
BEGIN
SELECT * FROM Concepto
END 

GO
USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[detallerecibo_BUSCAR]    Script Date: 30/9/2020 11:27:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[detallerecibo_BUSCAR]
           @numero int
as
BEGIN 


select* from DetalleRecibo where Recibo =@numero


END
GO
USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[detallerecibo_INSERTAR]    Script Date: 30/9/2020 11:27:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[detallerecibo_INSERTAR]
           @recibo int,
           @concepto int,
           @monto  float
as
BEGIN 


insert DetalleRecibo values (@recibo,@concepto,@monto)


END
GO
USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[EMPLEADO_BORRAR]    Script Date: 30/9/2020 11:27:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[EMPLEADO_BORRAR]
@legajo int
as
begin

delete from empleado 
where
legajo = @legajo 

end
GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[EMPLEADO_Buscar]    Script Date: 30/9/2020 11:27:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create PROC [dbo].[EMPLEADO_Buscar]
@legajo int
as
BEGIN
SELECT * FROM Empleado where Legajo = @legajo
END 
GO
USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[EMPLEADO_EDITAR]    Script Date: 30/9/2020 11:27:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[EMPLEADO_EDITAR]
		@legajo int,
           @Nombre nchar(10),
           @Apellido nchar(10),
           @Cuil nchar(13),
           @FechaDeAlta date,
           @SueldoBasico float

as
BEGIN 



update Empleado set Nombre=@Nombre,
Apellido=@Apellido,
Cuil=@Cuil,
FechaDeAlta=@FechaDeAlta,
SueldoBasico=@SueldoBasico

where
Legajo = @legajo 



END
GO
USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[EMPLEADO_INSERTAR]    Script Date: 30/9/2020 11:27:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[EMPLEADO_INSERTAR]
           @Nombre nchar(10),
           @Apellido nchar(10),
           @Cuil nchar(13),
           @FechaDeAlta date,
           @SueldoBasico float
as
BEGIN 
declare @legajo int

set @legajo  = isnull( (SELECT MAX(Legajo) from Empleado) ,0) +1

insert Empleado values (@legajo,@Nombre,@Apellido,@Cuil,@FechaDeAlta,@SueldoBasico)


END
GO
USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[EMPLEADO_LISTAR]    Script Date: 30/9/2020 11:27:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[EMPLEADO_LISTAR]
as
BEGIN
SELECT * FROM Empleado
END 

GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[RECIBO_BORRAR]    Script Date: 30/9/2020 11:27:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[RECIBO_BORRAR]
@numero int
as
begin

delete from Recibo 
where
numero = @numero 

end
GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[RECIBO_Buscar]    Script Date: 30/9/2020 11:28:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE   PROC [dbo].[RECIBO_Buscar]
@legajo int
as
BEGIN
SELECT * FROM Recibo where legajo = @legajo
END 

GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[RECIBO_Buscarporlegajo]    Script Date: 30/9/2020 11:28:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




create   PROC [dbo].[RECIBO_Buscarporlegajo]
@legajo int
as
BEGIN
SELECT * FROM Recibo where legajo = @legajo
END 

GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[RECIBO_Buscarpornumero]    Script Date: 30/9/2020 11:28:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create   PROC [dbo].[RECIBO_Buscarpornumero]
@numero int
as
BEGIN
SELECT * FROM Recibo where numero = @numero
END 

GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[RECIBO_EDITAR]    Script Date: 30/9/2020 11:28:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[RECIBO_EDITAR]
		@numero int,
		@mes int,
		@a�o int,
		@legajo int,
		@sueldobruto float,
		@sueldoneto float

as
BEGIN 



update Recibo set mes=@mes,
A�o=@a�o,
legajo=@legajo,
sueldobruto=@sueldobruto,
Sueldoneto=@sueldoneto

where
numero = @numero 



END
GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[RECIBO_INSERTAR]    Script Date: 30/9/2020 11:28:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[RECIBO_INSERTAR]
		@mes int,
		@a�o int,
		@legajo int,
		@sueldobruto float,
		@sueldoneto float
as
BEGIN 
declare @numero int


set @numero  = isnull( (SELECT MAX(numero) from Recibo) ,0) +1

insert Recibo values (@numero,@mes,@a�o,@legajo,@sueldobruto,@sueldoneto)

return @numero

END
GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[RECIBO_LISTAR]    Script Date: 30/9/2020 11:28:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[RECIBO_LISTAR]
as
BEGIN
SELECT * FROM Recibo
END 

GO

USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[RECIBO_ULTIMO]    Script Date: 30/9/2020 11:28:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[RECIBO_ULTIMO]
as
begin

Select ISNULL ( MAX(numero) , 0) +1 From Recibo

end
GO










