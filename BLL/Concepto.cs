﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Concepto
    {
        DAL.MP_Concepto mp = new DAL.MP_Concepto();

        public void Grabar(BE.Concepto Concepto)
        {
            if (Concepto.Codigo == 0)
            {
                mp.Insertar(Concepto);

            }
            else
            {
                mp.Editar(Concepto);
            }

        }

        public void Borrar(BE.Concepto Concepto)
        {
            mp.Borrar(Concepto);
        }

        public List<BE.Concepto> Listar()
        {
            return mp.Listar();

        }
        public List<BE.Concepto> getconcepto(bool Retencion)
        {
            return mp.Getconcepto(Retencion);

        }








    }
}
