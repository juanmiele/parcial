﻿using BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace BLL
{
    public class impresion

    {
        DAL.MP_Recibo mp = new DAL.MP_Recibo();
        DAL.MP_Empleado mpe = new DAL.MP_Empleado();
        DAL.MP_Concepto mpc = new DAL.MP_Concepto();
        DAL.MP_Detallerecibo mpd = new DAL.MP_Detallerecibo();
        public BE.Impresion generarencabezado(BE.Recibo recibo)
        {
            BE.Impresion impresion = new BE.Impresion();
            BE.Empleado empleado = mpe.GetEmpleado(recibo.Legajo);
            impresion.Legajo = empleado.Legajo;
            impresion.Nombre = empleado.Nombre;
            impresion.Apellido = empleado.Apellido;
            impresion.Cuil = empleado.Cuil;
            impresion.SueldoBasico = empleado.SueldoBasico;
            impresion.Fechadealta = empleado.Fechadealta;
            impresion.Numerorecibo = recibo.Numero;
            impresion.Sueldobruto = recibo.Sueldobruto;
            impresion.Sueldoneto = recibo.Sueldoneto;
            impresion.Liquidacion = (recibo.Mes.ToString()+'/' + recibo.Año.ToString());

            return impresion;
        }
        public BE.Impresion generardetalle(BE.Impresion impresion)
        {
           
            float totaldedescuentos = 0;
            List<BE.Detallerecibo> listadetalle = mpd.Listarporecibo(impresion.Numerorecibo);
            List<BE.Concepto> listaretenciones = mpc.Getconcepto(true);
            List<BE.Concepto> listaadicionales = mpc.Getconcepto(false);
            BE.Detalleimpresion detalleimpresion = new BE.Detalleimpresion();
            detalleimpresion.Porcentaje = 100;
            detalleimpresion.Nombre = "sueldo basico";
            detalleimpresion.Adicional = impresion.SueldoBasico.ToString("N2");
            impresion.Detallesimpreso.Add(detalleimpresion);
            detalleimpresion = new BE.Detalleimpresion();
            foreach (BE.Detallerecibo detalle in listadetalle)
            {
                //detalleimpresion = new BE.Detalleimpresion();
                foreach (BE.Concepto adicional in listaadicionales)
                {
                    if (detalle.Concepto == adicional.Codigo)
                    {
                        float monto = detalle.Monto;
                        detalleimpresion.Concepto = adicional.Codigo.ToString();
                        detalleimpresion.Nombre = adicional.Nombre.ToString();
                        detalleimpresion.Porcentaje = adicional.Porcentaje;
                        detalleimpresion.Adicional = detalle.Monto.ToString("N2");
                        detalleimpresion.Descuento = "";
                        impresion.Detallesimpreso.Add(detalleimpresion);
                        detalleimpresion = new BE.Detalleimpresion();
                    }
                }
            }

            foreach (BE.Detallerecibo detalle in listadetalle)
            {
                //detalleimpresion = new BE.Detalleimpresion();
                foreach (BE.Concepto retencion in listaretenciones)
                {
                    if (detalle.Concepto == retencion.Codigo)
                    {
                        float monto = detalle.Monto;
                        detalleimpresion.Concepto = retencion.Codigo.ToString();
                        detalleimpresion.Nombre = retencion.Nombre.ToString();
                        detalleimpresion.Porcentaje = retencion.Porcentaje;
                        detalleimpresion.Descuento = detalle.Monto.ToString("N2");
                        detalleimpresion.Adicional = "";
                        totaldedescuentos = (totaldedescuentos + (monto));
                        impresion.Detallesimpreso.Add(detalleimpresion);
                        detalleimpresion = new BE.Detalleimpresion();
                    }
                }
            }
            impresion.Totaldescuentos = totaldedescuentos.ToString("N2");
            return impresion;
        }
    }

 }

