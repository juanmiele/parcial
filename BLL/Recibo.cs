﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Recibo
    {
        DAL.MP_Recibo mp = new DAL.MP_Recibo();
        DAL.MP_Empleado mpe = new DAL.MP_Empleado();
        DAL.MP_Concepto mpc = new DAL.MP_Concepto();


        public void Grabar(BE.Recibo Recibo)
        {
                mp.Insertar(Recibo);               
        }

        public void Borrar(BE.Recibo Recibo)
        {
            mp.Borrar(Recibo);
        }

        public List<BE.Recibo> Listar()
        {
            return mp.Listar();

        }

        public void calcularbruto(BE.Recibo recibo)
        {
            bool descuento = false;
            float sueldobruto;
            float adicionales = 0;
            float porcentaje;

            BE.Empleado empleado = mpe.GetEmpleado(recibo.Legajo);
            List<BE.Concepto> listaconceptos = new List<BE.Concepto>();
            listaconceptos = mpc.Getconcepto(descuento);
            sueldobruto = empleado.SueldoBasico;
            foreach (BE.Concepto c in listaconceptos)
            {
                BE.Detallerecibo detallerecibos = new BE.Detallerecibo();
                porcentaje = c.Porcentaje;
                adicionales = adicionales+((porcentaje) /100)*sueldobruto;
                detallerecibos.Concepto = c.Codigo;
                detallerecibos.Recibo = recibo.Numero;
                detallerecibos.Monto = ((porcentaje) / 100) * sueldobruto;
                recibo.Detallerecibos.Add(detallerecibos);
            }
            recibo.Sueldobruto = sueldobruto+adicionales;
            

        }
        public void calcularneto(BE.Recibo recibo)
        {
            bool descuento = true;
            float sueldobruto;
            float porcentaje;
            float descuentos = 0;
            DAL.MP_Concepto mpc = new DAL.MP_Concepto();
            List<BE.Concepto> listaconceptos = new List<BE.Concepto>();
            listaconceptos = mpc.Getconcepto(descuento);
            sueldobruto = recibo.Sueldobruto;
            foreach (BE.Concepto c in listaconceptos)
            {
                BE.Detallerecibo detallerecibos = new BE.Detallerecibo();
                porcentaje = c.Porcentaje;
                descuentos = descuentos + (((porcentaje) / 100) * sueldobruto);
                detallerecibos.Concepto = c.Codigo;
                detallerecibos.Recibo = recibo.Numero;
                detallerecibos.Monto = ((porcentaje) / 100) * sueldobruto;
                recibo.Detallerecibos.Add(detallerecibos);
            }
            recibo.Sueldoneto = sueldobruto - descuentos;
        }
        public  BE.Recibo  Generarrecibo()
        {
            return  mp.NuevoRecibo();

        }
        public List<BE.Recibo> Listarporempleado(int legajo)
        {
            return mp.Listarporempleado(legajo);

        }
        public BE.Recibo Buscarpornumero(int numero)
        {
            return mp.Buscarpornumero(numero);

        }
    }
}
