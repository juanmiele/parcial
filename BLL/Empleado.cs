﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Empleado
    {
        DAL.MP_Empleado mp = new DAL.MP_Empleado();

        public void Grabar(BE.Empleado Empleado)
        {
            if (Empleado.Legajo == 0)
            {
                mp.Insertar(Empleado);

            }
            else
            {
                mp.Editar(Empleado);
            }

        }

        public void Borrar(BE.Empleado Empleado)
        {
            mp.Borrar(Empleado);
        }

        public List<BE.Empleado> Listar()
        {
            return mp.Listar();

        }
        public BE.Empleado getempeado(int legajo)
        {
            return mp.GetEmpleado(legajo);

        }
		public bool Validarcuit(string cuit)
			{
				if (string.IsNullOrEmpty(cuit)) throw new ArgumentNullException(nameof(cuit));
				if (cuit.Length != 13) throw new ArgumentException(nameof(cuit));
				bool rv = false;
				int verificador;
				int resultado = 0;
				string cuit_nro = cuit.Replace("-", string.Empty);
				string codes = "6789456789";
				long cuit_long = 0;
				if (long.TryParse(cuit_nro, out cuit_long))
				{
					verificador = int.Parse(cuit_nro[cuit_nro.Length - 1].ToString());
					int x = 0;
					while (x < 10)
					{
						int digitoValidador = int.Parse(codes.Substring((x), 1));
						int digito = int.Parse(cuit_nro.Substring((x), 1));
						int digitoValidacion = digitoValidador * digito;
						resultado += digitoValidacion;
						x++;
					}
					resultado = resultado % 11;
					rv = (resultado == verificador);
				}
				return rv;
		}
        public  bool IsDate(Object obj)
        {
            string strDate = obj.ToString();
            try
            {
                DateTime dt = DateTime.Parse(strDate);
                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}
