﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MP_Detallerecibo
    {
        private Acceso acceso = new Acceso();

        public void Insertar(Recibo recibo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            foreach (BE.Detallerecibo c in recibo.Detallerecibos)
            {
                parameters = new List<IDbDataParameter>
                {
                    acceso.CrearParametro("recibo", c.Recibo.ToString()),
                    acceso.CrearParametro("concepto", c.Concepto.ToString()),
                    acceso.CrearParametro("monto", c.Monto)
                };
                acceso.Escribir("detallerecibo_INSERTAR", parameters);
            }
            
            acceso.Cerrar();

        }

        public void Editar(BE.Detallerecibo detallerecibo)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@recibo", detallerecibo.Recibo));
            parameters.Add(acceso.CrearParametro("@concepto", detallerecibo.Concepto));
            parameters.Add(acceso.CrearParametro("@monto", detallerecibo.Monto));
            acceso.Escribir("detallerecibo_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Detallerecibo detallerecibo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@codigo", detallerecibo.Recibo));
            parameters.Add(acceso.CrearParametro("@codigo", detallerecibo.Concepto));
            acceso.Escribir("detallerecibo_BORRAR", parameters);

            acceso.Cerrar();
        }

        public List<BE.Detallerecibo> Listar()
        {
            List<BE.Detallerecibo> lista = new List<BE.Detallerecibo>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("detallerecibo_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Detallerecibo detallerecibo = new BE.Detallerecibo();
                detallerecibo.Recibo = int.Parse(registro[0].ToString());
                detallerecibo.Concepto = int.Parse(registro[1].ToString());
                detallerecibo.Monto = float.Parse(registro[2].ToString());
                lista.Add(detallerecibo);
            }
            return lista;
        }
            public List<BE.Detallerecibo> Listarporecibo(int numero)
        {
            List<BE.Detallerecibo> lista = new List<BE.Detallerecibo>();

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@numero", numero));
            DataTable tabla = acceso.Leer("detallerecibo_BUSCAR",parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Detallerecibo detallerecibo = new BE.Detallerecibo();
                detallerecibo.Recibo = int.Parse(registro[0].ToString());
                detallerecibo.Concepto = int.Parse(registro[1].ToString());
                detallerecibo.Monto = float.Parse(registro[2].ToString());
                lista.Add(detallerecibo);
            }
            return lista;
        }
    }
}
