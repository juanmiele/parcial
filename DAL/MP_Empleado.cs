﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
   public  class MP_Empleado
    {

        private Acceso acceso = new Acceso();

        public void Insertar( BE.Empleado Empleado )
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nombre", Empleado.Nombre));
            parameters.Add(acceso.CrearParametro("@apellido", Empleado.Apellido));
            parameters.Add(acceso.CrearParametro("@cuil", Empleado.Cuil));
            parameters.Add(acceso.CrearParametro("@fechadealta", Empleado.Fechadealta));
            parameters.Add(acceso.CrearParametro("@SueldoBasico", Empleado.SueldoBasico));
            acceso.Escribir("EMPLEADO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Empleado Empleado )
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", Empleado.Legajo));
            parameters.Add(acceso.CrearParametro("@nombre", Empleado.Nombre));
            parameters.Add(acceso.CrearParametro("@apellido", Empleado.Apellido));
            parameters.Add(acceso.CrearParametro("@cuil", Empleado.Cuil));
            parameters.Add(acceso.CrearParametro("@fechadealta", Empleado.Fechadealta));
            parameters.Add(acceso.CrearParametro("@SueldoBasico", Empleado.SueldoBasico));
            acceso.Escribir("Empleado_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Empleado Empleado)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", Empleado.Legajo));
            acceso.Escribir("Empleado_BORRAR", parameters);

            acceso.Cerrar();
        }

        public  List<BE.Empleado> Listar()
        {
            List<BE.Empleado> lista = new List<BE.Empleado>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("EMPLEADO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Empleado p = new BE.Empleado();
                p.Legajo = int.Parse(registro[0].ToString());
                p.Apellido = registro[1].ToString();
                p.Nombre = registro[2].ToString();
                p.Cuil = registro[3].ToString();
                p.Fechadealta = DateTime.Parse(registro[4].ToString());
                p.SueldoBasico = float.Parse(registro[5].ToString());
                lista.Add(p);
            }
            return lista;
        }
        
        public  BE.Empleado GetEmpleado (int legajo)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Legajo", legajo));
            DataTable tabla = acceso.Leer("EMPLEADO_BUSCAR", parameters);
            acceso.Cerrar();

            BE.Empleado empleado = new BE.Empleado();
            DataRow registro = tabla.Rows[0];

            empleado.Legajo = int.Parse(registro["legajo"].ToString());
            empleado.Apellido = registro["apellido"].ToString();
            empleado.Nombre = registro["nombre"].ToString();
            empleado.Cuil = registro["cuil"].ToString();
            empleado.Fechadealta = DateTime.Parse(registro["fechadealta"].ToString());
            empleado.SueldoBasico = float.Parse(registro["sueldobasico"].ToString());
            return empleado;

        }



    }
}
