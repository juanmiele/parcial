﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Concepto
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Concepto concepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nombre", concepto.Nombre));
            parameters.Add(acceso.CrearParametro("@porcentaje", concepto.Porcentaje.ToString()));
            parameters.Add(acceso.CrearParametro("@retencion", concepto.Retencion));
            acceso.Escribir("CONCEPTO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Concepto concepto)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@codigo", concepto.Codigo));
            parameters.Add(acceso.CrearParametro("@nombre", concepto.Nombre));
            parameters.Add(acceso.CrearParametro("@porcentaje", concepto.Porcentaje.ToString()));
            parameters.Add(acceso.CrearParametro("@retencion", concepto.Retencion));
            acceso.Escribir("CONCEPTO_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Concepto concepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@codigo", concepto.Codigo));
            acceso.Escribir("CONCEPTO_BORRAR", parameters);

            acceso.Cerrar();
        }

        public List<BE.Concepto> Listar()
        {
            List<BE.Concepto> lista = new List<BE.Concepto>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("CONCEPTO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Concepto c = new BE.Concepto();
                c.Codigo = int.Parse(registro[0].ToString());
                c.Nombre = registro[1].ToString();
                c.Porcentaje = int.Parse(registro[2].ToString());
                c.Retencion = bool.Parse(registro[3].ToString());
                lista.Add(c);
            }
            return lista;
        }
        public List<BE.Concepto> Getconcepto(bool Retencion)
        {
            List<BE.Concepto> lista = new List<BE.Concepto>();
            Acceso acceso = new Acceso(); 

            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@retencion", Retencion));
            DataTable tabla = acceso.Leer("CONCEPTO_BUSCAR", parameters);

            acceso.Cerrar();
            foreach (DataRow registro in tabla.Rows)
            {
                BE.Concepto c = new BE.Concepto();
                c.Codigo = int.Parse(registro[0].ToString());
                c.Nombre = registro[1].ToString();
                c.Porcentaje= int.Parse(registro[2].ToString());
                c.Retencion = bool.Parse(registro[3].ToString());
                lista.Add(c);
            }
            return lista;

        }
    }
}
