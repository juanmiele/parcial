﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
   public  class MP_Recibo
    {

        private Acceso acceso = new Acceso();

        public void Insertar( BE.Recibo Recibo )
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@mes", Recibo.Mes));
            parameters.Add(acceso.CrearParametro("@año", Recibo.Año));
            parameters.Add(acceso.CrearParametro("@legajo", Recibo.Legajo));
            parameters.Add(acceso.CrearParametro("@sueldobruto", Recibo.Sueldobruto));
            parameters.Add(acceso.CrearParametro("@Sueldoneto", Recibo.Sueldoneto));
            acceso.Escribir("RECIBO_INSERTAR", parameters);

            acceso.Cerrar();

        }

        public void Editar(BE.Recibo Recibo )
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@numero", Recibo.Numero));
            parameters.Add(acceso.CrearParametro("@mes", Recibo.Mes));
            parameters.Add(acceso.CrearParametro("@año", Recibo.Año));
            parameters.Add(acceso.CrearParametro("@legajo", Recibo.Legajo));
            parameters.Add(acceso.CrearParametro("@sueldobruto", Recibo.Sueldobruto));
            parameters.Add(acceso.CrearParametro("@Sueldoneto", Recibo.Sueldoneto));
            acceso.Escribir("RECIBO_EDITAR", parameters);
            
            acceso.Cerrar();
        }

        public void Borrar(BE.Recibo Recibo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@numero", Recibo.Numero));
            acceso.Escribir("RECIBO_BORRAR", parameters);

            acceso.Cerrar();
        }

        public List<BE.Recibo> Listar()
        {
            List<BE.Recibo> lista = new List<BE.Recibo>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("RECIBO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Recibo recibo = new BE.Recibo();
                recibo.Numero = int.Parse(registro[0].ToString());
                recibo.Mes = int.Parse(registro[1].ToString());
                recibo.Año = int.Parse(registro[2].ToString());
                recibo.Legajo = int.Parse(registro[3].ToString());
                recibo.Sueldobruto = float.Parse(registro[4].ToString());
                recibo.Sueldoneto = float.Parse(registro[5].ToString());
                lista.Add(recibo);
            }
            return lista;
        }
            public List<BE.Recibo> Listarporempleado(int legajo)
            {
                List<BE.Recibo> lista = new List<BE.Recibo>();

                acceso.Abrir();
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(acceso.CrearParametro("@Legajo", legajo));
                DataTable tabla = acceso.Leer("RECIBO_Buscarporlegajo",parameters);
                acceso.Cerrar();

                foreach (DataRow registro in tabla.Rows)
                {
                    BE.Recibo recibo = new BE.Recibo();
                    recibo.Numero = int.Parse(registro[0].ToString());
                    recibo.Mes = int.Parse(registro[1].ToString());
                    recibo.Año = int.Parse(registro[2].ToString());
                    recibo.Legajo = int.Parse(registro[3].ToString());
                    recibo.Sueldobruto = float.Parse(registro[4].ToString());
                    recibo.Sueldoneto = float.Parse(registro[5].ToString());
                    lista.Add(recibo);
                }
                return lista;
            }
        public Recibo Buscarpornumero(int numero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@numero", numero));
            DataTable tabla = acceso.Leer("RECIBO_Buscarpornumero", parameters);
            acceso.Cerrar();
            BE.Recibo recibo = new BE.Recibo();
            foreach (DataRow registro in tabla.Rows)
            {

                recibo.Numero = int.Parse(registro[0].ToString());
                recibo.Mes = int.Parse(registro[1].ToString());
                recibo.Año = int.Parse(registro[2].ToString());
                recibo.Legajo = int.Parse(registro[3].ToString());
                recibo.Sueldobruto = float.Parse(registro[4].ToString());
                recibo.Sueldoneto = float.Parse(registro[5].ToString());
            }
            return recibo;
        }

        public Recibo NuevoRecibo()
        {
            acceso.Abrir();
            DataTable tabla = acceso.Leer("RECIBO_ULTIMO");
            acceso.Cerrar();

            BE.Recibo recibo = new BE.Recibo();
            foreach (DataRow registro in tabla.Rows)
            {
                recibo.Numero = int.Parse(registro[0].ToString());
            }
            return recibo;
        }






    }
}
