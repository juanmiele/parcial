﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Impresion
    {

        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string cuil;

        public string Cuil
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fechadealta;

        public DateTime Fechadealta
        {
            get { return fechadealta; }
            set { fechadealta = value; }
        }
        private float sueldoBasico;

        public float SueldoBasico
        {
            get { return sueldoBasico; }
            set { sueldoBasico = value; }
        }
        private float sueldoneto;

        public float Sueldoneto
        {
            get { return sueldoneto; }
            set { sueldoneto = value; }
        }
        private float sueldobruto;

        public float Sueldobruto
        {
            get { return sueldobruto; }
            set { sueldobruto = value; }
        }
        private int numerorecibo;

        public int Numerorecibo
        {
            get { return numerorecibo; }
            set { numerorecibo = value; }
        }
        private string liquidacion;
        public string Liquidacion
        {
            get { return liquidacion; }
            set { liquidacion = value; }
        }
        private string totaldescuentos;
        public string Totaldescuentos
        {
            get { return totaldescuentos; }
            set { totaldescuentos = value; }
        }

        private List<Detalleimpresion> detallesimpreso = new List<Detalleimpresion>();

        public List<Detalleimpresion> Detallesimpreso
        {
            get { return detallesimpreso; }
        }

    }
}
