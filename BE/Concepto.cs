﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Concepto
    {
        private int codigo;

        public int Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private int porcentaje;

        public int Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }
        private  bool retencion;

        public bool Retencion
        {
            get { return retencion; }
            set { retencion = value; }
        }
    }
}
