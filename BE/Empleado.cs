﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Empleado
    {

        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string cuil;

        public string Cuil
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fechadealta;

        public DateTime Fechadealta
        {
            get { return fechadealta; }
            set { fechadealta = value; }
        }
        private float sueldoBasico;

        public float SueldoBasico
        {
            get { return sueldoBasico; }
            set { sueldoBasico = value; }
        }

    }
}
