﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Recibo
    {
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        private int mes;

        public int Mes
        {
            get { return mes; }
            set { mes = value; }
        }
        private int año;

        public int Año
        {
            get { return año; }
            set { año = value; }
        }
        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private float sueldobruto;

        public float Sueldobruto
        {
            get { return sueldobruto; }
            set { sueldobruto = value; }
        }
        private float sueldoneto;

        public float Sueldoneto
        {
            get { return sueldoneto; }
            set { sueldoneto = value; }
        }
        private List<Concepto> conceptos;
            
            public List<Concepto> Conceptos
        {
            get { return conceptos; }
            set { conceptos = value; }
        }

        private List<Detallerecibo> detallerecibos = new List<Detallerecibo>();

        public List<Detallerecibo> Detallerecibos
        {
            get { return detallerecibos; }
        }

    }
}
