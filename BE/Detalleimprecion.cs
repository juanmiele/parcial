﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Detalleimpresion
    {


        private string concepto;

        public string Concepto
        {
            get { return concepto; }
            set { concepto = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private int porcentaje;

        public int Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }

        private string adicional;

        public string Adicional
        {
            get { return adicional; }
            set { adicional = value; }
        }
        private string descuento;

        public string Descuento
        {
            get { return descuento; }
            set { descuento = value; }
        }

    }
}
