USE [Recibo]
GO

/****** Object:  StoredProcedure [dbo].[CONCEPTO_BORRAR]    Script Date: 30/9/2020 11:25:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[CONCEPTO_BORRAR]
@codigo int
as
begin

delete from Concepto 
where
Codigo = @codigo

end
GO


USE [Recibo]
GO

/****** Object:  Table [dbo].[DetalleRecibo]    Script Date: 30/9/2020 11:25:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DetalleRecibo](
	[recibo] [int] NOT NULL,
	[concepto] [int] NOT NULL,
	[monto] [float] NULL,
 CONSTRAINT [PK_DetalleRecibo] PRIMARY KEY CLUSTERED 
(
	[recibo] ASC,
	[concepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DetalleRecibo]  WITH CHECK ADD  CONSTRAINT [FK_DetalleRecibo_Concepto] FOREIGN KEY([concepto])
REFERENCES [dbo].[Concepto] ([Codigo])
GO

ALTER TABLE [dbo].[DetalleRecibo] CHECK CONSTRAINT [FK_DetalleRecibo_Concepto]
GO

ALTER TABLE [dbo].[DetalleRecibo]  WITH CHECK ADD  CONSTRAINT [FK_DetalleRecibo_Recibo] FOREIGN KEY([recibo])
REFERENCES [dbo].[Recibo] ([numero])
GO

ALTER TABLE [dbo].[DetalleRecibo] CHECK CONSTRAINT [FK_DetalleRecibo_Recibo]
GO
USE [Recibo]
GO

/****** Object:  Table [dbo].[Empleado]    Script Date: 30/9/2020 11:25:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Empleado](
	[Legajo] [int] NOT NULL,
	[Nombre] [nchar](50) NULL,
	[Apellido] [nchar](10) NULL,
	[Cuil] [nchar](13) NULL,
	[FechaDeAlta] [date] NULL,
	[SueldoBasico] [float] NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[Legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [Recibo]
GO

/****** Object:  Table [dbo].[Recibo]    Script Date: 30/9/2020 11:26:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Recibo](
	[numero] [int] NOT NULL,
	[mes] [int] NULL,
	[a�o] [int] NULL,
	[legajo] [int] NULL,
	[SueldoBruto] [float] NULL,
	[SueldoNeto] [float] NULL,
 CONSTRAINT [PK_Recibo] PRIMARY KEY CLUSTERED 
(
	[numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Recibo]  WITH CHECK ADD  CONSTRAINT [FK_Recibo_Empleado] FOREIGN KEY([legajo])
REFERENCES [dbo].[Empleado] ([Legajo])
GO

ALTER TABLE [dbo].[Recibo] CHECK CONSTRAINT [FK_Recibo_Empleado]
GO



