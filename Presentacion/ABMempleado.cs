﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class ABMempleado : Form
    {

        BLL.Empleado gestore = new BLL.Empleado();
        

        public ABMempleado()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            limpiar();
        }

        private void Enlazar()
        {
            DataGridView1.DataSource = null;
            DataGridView1.DataSource = gestore.Listar();


        }
        private void limpiar()
        {
            Enlazar();
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "dd/MM/yyyy";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            BE.Empleado p = new BE.Empleado();

            p.Nombre = textBox2.Text;
            if (string.IsNullOrWhiteSpace(textBox2.Text))
            {
                MessageBox.Show(
                 "El nombre esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }    
            else
            { 
            p.Apellido = textBox3.Text;
                if (string.IsNullOrWhiteSpace(textBox3.Text))
                {
                    MessageBox.Show(
                   "El apellido esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(textBox8.Text) | string.IsNullOrWhiteSpace(textBox4.Text) | string.IsNullOrWhiteSpace(textBox8.Text))
                    {
                        MessageBox.Show(
                        "El cuil es incorrecto, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        p.Cuil = textBox8.Text.Substring(0, 2) + '-' + textBox4.Text.Substring(0, 8) + '-' + textBox7.Text.Substring(0, 1);
                        string cuit = p.Cuil;
                        if (cuit.Length != 15)
                        {
                            if (gestore.Validarcuit(cuit) == true)
                            {

                                if (gestore.IsDate(textBox5.Text) == true)
                                {
                                    p.Fechadealta = Convert.ToDateTime(textBox5.Text);
                                    if (string.IsNullOrWhiteSpace(textBox6.Text))
                                    {
                                        MessageBox.Show(
                                       "El Sueldo Basico esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    }
                                    else
                                    {
                                        if (float.Parse(textBox6.Text) > 0)
                                        {
                                            p.SueldoBasico = float.Parse(textBox6.Text);
                                            gestore.Grabar(p);
                                            limpiar();
                                        }
                                        else
                                        {
                                            MessageBox.Show(
                                            "El Sueldo basico es incorrecto, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                        }

                                    }
                                }
                                else
                                {
                                    MessageBox.Show(
                                    "La fecha es incorrecta, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show(
                                "El cuil es incorrecto, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show(
                            "El cuil es incorrecto, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        
                    }

                }


             }
     
         
       }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (radioButton2.Checked == true | radioButton3.Checked == true)
            {
                textBox1.Text = DataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                textBox2.Text = DataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                textBox3.Text = DataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
                textBox4.Text = DataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
                textBox5.Text = ((DateTime)DataGridView1.Rows[e.RowIndex].Cells[4].Value).ToString("dd/MM/yyyy");
                textBox6.Text = DataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            }
        }



        private void button2_Click(object sender, EventArgs e)
        {
            BE.Empleado p = new BE.Empleado();

            p.Legajo = int.Parse(textBox1.Text);
            p.Nombre = textBox2.Text;
            if (string.IsNullOrWhiteSpace(textBox2.Text))
            {
                MessageBox.Show(
                 "El nombre esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                p.Apellido = textBox3.Text;
                if (string.IsNullOrWhiteSpace(textBox3.Text))
                {
                    MessageBox.Show(
                   "El apellido esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(textBox8.Text) | string.IsNullOrWhiteSpace(textBox4.Text) | string.IsNullOrWhiteSpace(textBox8.Text))
                    {
                        MessageBox.Show(
                        "El cuil es incorrecto, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        p.Cuil = textBox8.Text.Substring(0, 2) + '-' + textBox4.Text.Substring(0, 8) + '-' + textBox7.Text.Substring(0, 1);
                        string cuit = p.Cuil;
                        if (cuit.Length != 15)
                        {
                            if (gestore.Validarcuit(cuit) == true)
                            {

                                if (gestore.IsDate(textBox5.Text) == true)
                                {
                                    p.Fechadealta = Convert.ToDateTime(textBox5.Text);
                                    if (string.IsNullOrWhiteSpace(textBox6.Text))
                                    {
                                        MessageBox.Show(
                                       "El Sueldo Basico esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    }
                                    else
                                    {
                                        if (float.Parse(textBox6.Text) > 0)
                                        {
                                            p.SueldoBasico = float.Parse(textBox6.Text);
                                            gestore.Grabar(p);
                                            limpiar();
                                        }
                                        else
                                        {
                                            MessageBox.Show(
                                            "El Sueldo basico es incorrecto, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                        }

                                    }
                                }
                                else
                                {
                                    MessageBox.Show(
                                    "La fecha es incorrecta, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show(
                                "El cuil es incorrecto, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show(
                            "El cuil es incorrecto, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                    }

                }


            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show(
                    "Elija un empleado de la grilla", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            { 
                BE.Empleado p = new BE.Empleado();

                p.Legajo = int.Parse(textBox1.Text);

                gestore.Borrar(p);
                limpiar();
                Enlazar();
            }
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }



        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            limpiar();
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = true;
            label8.Visible = true;
            textBox1.Visible = true;
            textBox1.Enabled = false;
            textBox2.Visible = true;
            textBox2.Enabled = true;
            textBox3.Visible = true;
            textBox3.Enabled = true;
            textBox4.Visible = true;
            textBox4.Enabled = true;
            textBox5.Visible = true;
            textBox5.Enabled = true;
            textBox6.Visible = true;
            textBox6.Enabled = true;
            button1.Visible = true;
            DataGridView1.Visible = true;
         

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            limpiar();
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = true;
            label8.Visible = true;
            textBox1.Visible = true;
            textBox1.Enabled = false;
            textBox2.Visible = true;
            textBox2.Enabled = false;
            textBox3.Visible = true;
            textBox3.Enabled = false;
            textBox4.Visible = true;
            textBox4.Enabled = false;
            textBox5.Visible = true;
            textBox5.Enabled = false;
            textBox6.Visible = true;
            textBox6.Enabled = false;
            button3.Visible = true;
            DataGridView1.Visible = true;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            limpiar();
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = true;
            label8.Visible = true;
            textBox1.Visible = true;
            textBox1.Enabled = false;
            textBox2.Visible = true;
            textBox2.Enabled = true;
            textBox3.Visible = true;
            textBox3.Enabled = true;
            textBox4.Visible = true;
            textBox4.Enabled = true;
            textBox5.Visible = true;
            textBox5.Enabled = true;
            textBox6.Visible = true;
            textBox6.Enabled = true;
            button2.Visible = true;
            DataGridView1.Visible = true;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
