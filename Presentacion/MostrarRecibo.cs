﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class MostrarRecibo : Form
    {
        public MostrarRecibo()
        {
            InitializeComponent();
        }
        BLL.Empleado gestore = new BLL.Empleado();
        BLL.Recibo gestorr = new BLL.Recibo();
        BLL.Detallerecibo gestord = new BLL.Detallerecibo();
        BLL.impresion gestori = new BLL.impresion();
       
        private void Enlazar()
        {
            DataGridView3.DataSource = null;
            DataGridView3.DataSource = gestore.Listar();
        }

        private void MostrarRecibo_Load_1(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void DataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView4.DataSource = null;
            DataGridView4.DataSource = gestorr.Listarporempleado(int.Parse(DataGridView3.Rows[e.RowIndex].Cells[0].Value.ToString()));
            DataGridView4.Visible = true;
            textBox1.Text = "";
            button1.Visible = false;
            label13.Visible = true;
            dataGridView1.Visible = false;
            groupBox1.Visible = false;
            groupBox2.Visible = false;
            textBox1.Text = DataGridView3.Rows[e.RowIndex].Cells[0].Value.ToString();

        }

        private void DataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            List<BE.Detallerecibo> lista = new List<BE.Detallerecibo>();
            textBox1.Text = DataGridView4.Rows[e.RowIndex].Cells[0].Value.ToString();
            button1.Visible=true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                //mesnsaje
            }
            else
            {
                BE.Recibo recibo = new BE.Recibo();
                recibo = gestorr.Buscarpornumero(int.Parse(textBox1.Text));
                BE.Impresion impresion = new BE.Impresion();
                impresion = gestori.generarencabezado(recibo);
                impresion = gestori.generardetalle(impresion);
                groupBox1.Visible = true;
                groupBox2.Visible = true;
                dataGridView1.Visible = true;
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = impresion.Detallesimpreso;
                label1.Text = impresion.Numerorecibo.ToString();
                label14.Text = impresion.Liquidacion.ToString();
                label4.Text = impresion.Apellido.ToString() + impresion.Nombre.ToString();
                label5.Text = impresion.Cuil.ToString();
                label6.Text = impresion.Legajo.ToString();
                label7.Text = impresion.Sueldobruto.ToString("N2");
                label8.Text = impresion.Sueldoneto.ToString("N2");
                label21.Text = impresion.Totaldescuentos.ToString();
            }



        }


    }
}