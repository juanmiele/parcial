﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class ABMRecibo : Form
    {
        public ABMRecibo()
        {
            InitializeComponent();
        }
        BLL.Empleado gestore = new BLL.Empleado();
        BLL.Recibo gestorr = new BLL.Recibo();
        BLL.Detallerecibo gestord = new BLL.Detallerecibo();

        private void Enlazar()
        {
            DataGridView1.DataSource = null;
            DataGridView1.DataSource = gestore.Listar();



        }

        private void ABMRecibo_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = DataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox4.Text = DataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox5.Text = DataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            BE.Recibo recibo = new BE.Recibo();
            recibo = gestorr.Generarrecibo();
            if (textBox2.Text == "")
            {
                MessageBox.Show(
                 "El mes esta en blanco, favor de elija un mes de la lista ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                recibo.Mes = int.Parse(textBox2.Text);
                if (textBox3.Text == "")
                {
                    MessageBox.Show(
                     "El año esta en blanco, favor de elija un año de la lista ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    recibo.Año = int.Parse(textBox3.Text);
                    if (textBox1.Text == "")
                    {
                        MessageBox.Show(
                         "El legajo esta en blanco, favor de elija un empleado de la lista ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        recibo.Legajo = int.Parse(textBox1.Text);
                        gestorr.calcularbruto(recibo);
                        gestorr.calcularneto(recibo);
                        gestorr.Grabar(recibo);
                        gestord.Grabar(recibo);
                        MessageBox.Show(
                        "Felicitacion, se genero el recibo con exito ", "Recibo generado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        Enlazar();
                    }
                }
            }
        }



        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox2.Text = listBox1.Text;
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox3.Text = listBox2.Text;
        }


    }
}
