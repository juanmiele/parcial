﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class inicio : Form
    {
        public inicio()
        {
            InitializeComponent();
        }


        private void reciboToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMRecibo abmRecibo = new ABMRecibo();

            abmRecibo.Show();
        }

        private void empleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMempleado aBMempleado = new ABMempleado();

            aBMempleado.Show();
        }

        private void conceptoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMConsepto aBMConsepto = new ABMConsepto();
            aBMConsepto.Show();
        }

        private void imprimirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarRecibo mostrarrecibo = new MostrarRecibo();
            mostrarrecibo.Show();
        }

        private void reciboToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MostrarRecibo mostrarrecibo = new MostrarRecibo();
            mostrarrecibo.Show();
        }
    }
}
