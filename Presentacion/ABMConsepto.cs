﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class ABMConsepto : Form
    {
        BLL.Concepto gestorc = new BLL.Concepto();
        public ABMConsepto()
        {
            InitializeComponent();
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            limpiar();
        }
        private void Enlazar()
        {
            DataGridView2.Visible = true;
            DataGridView2.DataSource = null;
            DataGridView2.DataSource = gestorc.Listar();

        }
        private void limpiar()
        {
            DataGridView2.Visible = false;
            textBox7.Text = "";
            textBox7.Enabled = false;
            textBox8.Text = "";
            textBox9.Text = "";
            checkBox1.Checked = false;
            groupBox1.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            Enlazar();



        }

        private void button5_Click(object sender, EventArgs e)
        {
            BE.Concepto c = new BE.Concepto();

            c.Codigo = int.Parse(textBox7.Text);
            if (string.IsNullOrWhiteSpace(textBox8.Text))
                {
                    MessageBox.Show(
                     "El nombre esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    c.Nombre = textBox8.Text;
                }
                    if (string.IsNullOrWhiteSpace(textBox9.Text))
                    {
                        MessageBox.Show(
                         "El porcentaje esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    { c.Porcentaje = int.Parse(textBox9.Text); }

            c.Retencion = checkBox1.Checked;

            gestorc.Grabar(c);
            Enlazar();
        }



        private void button4_Click(object sender, EventArgs e)
        {
            BE.Concepto c = new BE.Concepto();

            c.Codigo = int.Parse(textBox7.Text);

            gestorc.Borrar(c);
            Enlazar();
        }

        private void DataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (radioButton2.Checked == true | radioButton3.Checked == true)
            {
                textBox7.Text = DataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString();
                textBox8.Text = DataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
                textBox9.Text = DataGridView2.Rows[e.RowIndex].Cells[2].Value.ToString();
                checkBox1.Checked = Convert.ToBoolean(DataGridView2.Rows[e.RowIndex].Cells[3].Value);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            BE.Concepto c = new BE.Concepto();
            if (string.IsNullOrWhiteSpace(textBox8.Text))
            {
                MessageBox.Show(
                 "El nombre esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                c.Nombre = textBox8.Text;
            }
            if (string.IsNullOrWhiteSpace(textBox9.Text))
            {
                MessageBox.Show(
                 "El porcentaje esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            { c.Porcentaje = int.Parse(textBox9.Text); }
            c.Retencion = checkBox1.Checked;
            gestorc.Grabar(c);

            Enlazar();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
        private void DataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox7.Text = DataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox8.Text = DataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox9.Text = DataGridView2.Rows[e.RowIndex].Cells[2].Value.ToString();
            checkBox1.Checked = Convert.ToBoolean(DataGridView2.Rows[e.RowIndex].Cells[3].Value);

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            limpiar();
            groupBox1.Visible = true;
            button6.Visible = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            limpiar();
            groupBox1.Visible = true;
            button5.Visible = true;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            limpiar();
            groupBox1.Visible = true;
            button4.Visible = true;
        }
    }
}
